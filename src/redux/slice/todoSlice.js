import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  todoItems: [],
};

const todoSlice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    addTodoItem: (state, action) => {},
    toggleItem: (state, action) => {
      let todoItem = state.todoItems.find(
        (todoItem) => todoItem.id === action.payload
      );
      todoItem.done = !todoItem.done;
    },
    deleteItem: (state, action) => {
      let todoItemIndex = state.todoItems.findIndex(
        (todoItem) => todoItem.id === action.payload
      );
      state.todoItems.splice(todoItemIndex, 1);
    },
    initialItems: (state, action) => {
      state.todoItems = action.payload;
    },
  },
});

export const { addTodoItem, toggleItem, deleteItem, initialItems } =
  todoSlice.actions;
export default todoSlice.reducer;
