import { RouterProvider } from "react-router-dom";
import router from "./routers/router";
import { useTodos } from "./hooks/useTodos";
import { useEffect } from "react";

function App() {
  const { loadTodos, contextHolder } = useTodos();

  useEffect(() => {
    loadTodos();
  }, []);

  return (
    <div className="h-screen flex flex-col">
      {contextHolder}
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
