import { Link, useLocation } from "react-router-dom";
import { QuestionCircleOutlined } from "@ant-design/icons";

const Navigation = () => {
  const { pathname } = useLocation();

  const navigation = [
    {
      name: "Home",
      to: "/",
      highlight: "/" === pathname,
    },
    {
      name: "Done",
      to: "/done",
      highlight: pathname.startsWith("/done"),
    },
  ];

  return (
    <nav className="flex p-3 bg-gradient-to-r from-cyan-500 to-blue-500 mb-10">
      <div className="mr-6 text-2xl text-white gap-5">Dashboard</div>
      <div className="flex items-center gap-10 text-lg justify-center mr-10">
        {navigation.map((page) => (
          <Link
            key={page.to}
            className={`${
              page.highlight ? "text-white" : "text-teal-200"
            } hover:text-white`}
            to={page.to}
          >
            {page.name}
          </Link>
        ))}
      </div>
      <Link
        className={`${
          pathname === "/help" ? "text-white" : "text-teal-200"
        } hover:text-white flex items-center gap-2 ml-auto`}
        to="/help"
      >
        <QuestionCircleOutlined />
        <p>Help Page</p>
      </Link>
    </nav>
  );
};

export default Navigation;
