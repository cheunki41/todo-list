import { useDispatch } from "react-redux";
import {
  getTodos,
  addTodo as addTodoItem,
  updateTodo as updateTodoItem,
  deleteTodo as deleteTodoItem,
} from "../api/todos";
import { initialItems } from "../redux/slice/todoSlice";
import { message } from "antd";

export const useTodos = () => {
  const dispatch = useDispatch();
  const [messageApi, contextHolder] = message.useMessage();

  const loadTodos = () => {
    messageApi
      .open({
        tpye: "loading",
        content: "Loading to-do items...",
        duration: 0.5,
      })
      .then(() => {
        getTodos().then((response) => {
          dispatch(initialItems(response.data));
        });
        message.success("Loading finished!", 0.5);
      });
  };

  const addTodo = async (text) => {
    messageApi
      .open({
        tpye: "loading",
        content: "Adding new to-do item...",
        duration: 0.5,
      })
      .then(async () => {
        await addTodoItem(text);
        loadTodos();
        message.success("Successfully added!", 0.5);
      });
  };

  const toggleTodo = async (id, done) => {
    messageApi
      .open({
        tpye: "loading",
        content: `Changing to-do item to ${
          done ? "finished" : "unfinished"
        } state...`,
        duration: 0.5,
      })
      .then(async () => {
        await updateTodoItem(id, {
          done: done,
        });
        loadTodos();
        message.success(
          `Successfully changed to ${done ? "finished" : "unfinished"} state`,
          0.5
        );
      });
  };

  const deleteTodo = async (id, done) => {
    messageApi
      .open({
        tpye: "loading",
        content: `Deleting to-do item`,
        duration: 0.5,
      })
      .then(async () => {
        await deleteTodoItem(id, done);
        loadTodos();
        message.success(`Successfully deleted!`, 0.5);
      });
  };

  const updateTodo = async (id, text) => {
    messageApi
      .open({
        tpye: "loading",
        content: `Updating to-do item`,
        duration: 0.5,
      })
      .then(async () => {
        await updateTodoItem(id, {
          text: text,
        });
        loadTodos();
        message.success(`Successfully updated!`, 0.5);
      });
  };

  return {
    contextHolder,
    loadTodos,
    addTodo,
    toggleTodo,
    deleteTodo,
    updateTodo,
  };
};
