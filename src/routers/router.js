import { createBrowserRouter } from "react-router-dom";
import HelpPage from "../components/HelpPage";
import DoneList from "../components/DoneList";
import Layout from "../layouts/Layout";
import TodoList from "../components/TodoList";
import TodoItemPage from "../components/TodoItemDetail";
import NotFoundPage from "../components/NotFoundPage";

const router = createBrowserRouter([
  {
    path: "",
    element: <Layout />,
    children: [
      {
        index: true,
        element: <TodoList />,
      },
      {
        path: "/done",
        element: <DoneList />,
      },
      {
        path: "/help",
        element: <HelpPage />,
      },
      {
        path: "/done/:id",
        element: <TodoItemPage />,
      },
      {
        path: "*",
        element: <NotFoundPage />,
      },
    ],
  },
]);

export default router;
