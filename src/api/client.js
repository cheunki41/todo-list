import axios from "axios";

const client = axios.create({
  baseURL: "https://6566e09564fcff8d730f323d.mockapi.io/api",
});

export default client;
