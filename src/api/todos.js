import client from "./client";

export const getTodos = () => {
  return client.get("/todos");
};

export const addTodo = (text) => {
  return client.post("/todos", {
    text: text,
    done: false,
  });
};

export const updateTodo = (id, payload) => {
  return client.put(`/todos/${id}`, payload);
};

export const deleteTodo = (id) => {
  return client.delete(`/todos/${id}`);
};
