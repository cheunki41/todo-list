import { useTodos } from "../hooks/useTodos";
import { Button, Modal, Input } from "antd";
import { DeleteFilled, EditOutlined } from "@ant-design/icons";
import { useState } from "react";

const TodoItem = ({ todoItem }) => {
  const { toggleTodo, deleteTodo, updateTodo, contextHolder } = useTodos();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isDeleteModalOpen, setIsDeleteModelOpen] = useState(false);
  const [itemText, setItemText] = useState(todoItem.text);

  const { TextArea } = Input;

  const onCloseModal = () => {
    setIsModalOpen(false);
  };

  const onOpenModal = () => {
    setIsModalOpen(true);
    setItemText(todoItem.text);
  };

  const handleToggleItem = () => {
    toggleTodo(todoItem.id, !todoItem.done);
  };

  const handleDeleteItem = () => {
    deleteTodo(todoItem.id);
  };

  const handleUpdateItem = async () => {
    if (itemText.trim() === "") return;
    await updateTodo(todoItem.id, itemText);
    onCloseModal();
  };

  const onHandleItemTextChange = (event) => {
    setItemText(event.target.value);
  };

  const onCloseDeleteModal = () => {
    setIsDeleteModelOpen(false);
  };

  const onOpenDeleteModel = () => {
    setIsDeleteModelOpen(true);
  };

  return (
    <div
      className={`flex flex-row border-solid ${
        todoItem.done && "border-green-300 hover:border-green-500"
      } hover:border-cyan-500 border-2 items-center w-[35rem] rounded-md gap-2 p-1`}
    >
      <div
        className="grow pl-3 pt-1 pb-1 overflow-hidden whitespace-nowrap text-ellipsis max-w-[29rem]"
        onClick={handleToggleItem}
      >
        {todoItem.text}
      </div>
      <Button onClick={onOpenModal} icon={<EditOutlined />} />
      <Button
        onClick={onOpenDeleteModel}
        icon={<DeleteFilled className="text-red-400" />}
      />
      <Modal
        title="Update Todo Item"
        open={isModalOpen}
        onOk={handleUpdateItem}
        onCancel={onCloseModal}
        okButtonProps={{
          disabled: itemText.trim() === "" || itemText === todoItem.text,
          ghost: true,
        }}
        centered
      >
        <TextArea
          cols={70}
          value={itemText}
          onChange={onHandleItemTextChange}
        />
      </Modal>
      <Modal
        title={<p className="text-red-400">Delete Todo Item</p>}
        open={isDeleteModalOpen}
        okButtonProps={{ danger: true }}
        okText="Confirm"
        onOk={handleDeleteItem}
        onCancel={onCloseDeleteModal}
        centered
      >
        Are you sure to delete this todo item ?
      </Modal>
      {contextHolder}
    </div>
  );
};

export default TodoItem;
