import { useTodos } from "../hooks/useTodos";
import { useState } from "react";
import { Button, Input } from "antd";

const TodoGenerator = () => {
  const { addTodo, contextHolder } = useTodos();
  const [inputItem, setInputItem] = useState("");

  const addItem = () => {
    if (inputItem === "") return;
    addTodo(inputItem);
    setInputItem("");
  };

  const handleInputItemChange = (event) => {
    setInputItem(event.target.value);
  };

  return (
    <div className="flex flex-row gap-5 w-[30rem]">
      <Input
        name="todoItem"
        placeholder="input a new todo here..."
        value={inputItem}
        onChange={handleInputItemChange}
      />
      <Button disabled={inputItem.trim() === ""} onClick={addItem}>
        Add
      </Button>
      {contextHolder}
    </div>
  );
};

export default TodoGenerator;
