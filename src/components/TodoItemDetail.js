import { useParams, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { Descriptions, Tooltip, Button } from "antd";
import { CheckCircleFilled, SwapLeftOutlined } from "@ant-design/icons";

const TodoItemPage = () => {
  const { id } = useParams();
  const todoItem = useSelector((state) => state.todo.todoItems).find(
    (item) => item.id === id
  );
  const items = [
    {
      key: "id",
      label: "Id",
      children: todoItem?.id,
      span: 3,
    },
    {
      key: "text",
      label: "Text",
      children: todoItem?.text,
      span: 3,
    },
    {
      key: "Status",
      label: "Status",
      children: (
        <>
          <CheckCircleFilled className="text-green-500 mr-3" /> Done
        </>
      ),
    },
  ];
  const navigate = useNavigate();

  const back = () => {
    navigate("/done");
  };
  return (
    <div className="flex flex-col pl-20 pr-20 gap-10">
      <div>
        <Tooltip placement="topLeft" title="Back">
          <Button onClick={back} shape="circle" icon={<SwapLeftOutlined />} />
        </Tooltip>
      </div>
      <Descriptions title="Todo Item Detail" bordered items={items} />
    </div>
  );
};

export default TodoItemPage;
