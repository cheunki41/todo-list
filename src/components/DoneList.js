import { useSelector } from "react-redux";
import { Badge, Empty } from "antd";
import { useNavigate } from "react-router-dom";

const Done = () => {
  const { todoItems } = useSelector((state) => state.todo);
  const doneItems = todoItems.filter((item) => item.done);
  const navigate = useNavigate();

  return (
    <div className="flex flex-col items-center gap-12">
      <div className="flex flex-row items-center gap-2 text-2xl">
        <p>To-do Done List</p>
        <Badge
          count={doneItems.length}
          color={doneItems.length > 0 && "green"}
        />
      </div>
      <div className="flex flex-col gap-5 items-center max-h-[45rem] overflow-y-auto">
        {doneItems.map((item) => (
          <div
            key={item.id}
            className="flex flex-row border-solid
               border-green-300 hover:border-green-500 border-2 items-center w-[35rem] rounded-md gap-2 p-1"
            onClick={() => navigate(`/done/${item.id}`)}
          >
            <div className="grow pl-3 pt-1 pb-1 overflow-hidden whitespace-nowrap text-ellipsis pr-3">
              {item.text}
            </div>
          </div>
        ))}
      </div>
      {doneItems.length == 0 && (
        <Empty
          image={Empty.PRESENTED_IMAGE_SIMPLE}
          description="No done items"
        />
      )}
    </div>
  );
};

export default Done;
