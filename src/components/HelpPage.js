import { Result } from "antd";
import { InfoCircleTwoTone } from "@ant-design/icons";

const HelpPage = () => {
  return (
    <div className="grow flex items-center justify-center">
      <Result
        icon={<InfoCircleTwoTone />}
        title="You may find something useful here!"
      />
    </div>
  );
};

export default HelpPage;
