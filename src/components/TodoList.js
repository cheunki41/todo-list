import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";
import { Badge } from "antd";
import { useSelector } from "react-redux";

const TodoList = () => {
  const { todoItems } = useSelector((state) => state.todo);
  const count = todoItems.filter((item) => !item.done).length;
  return (
    <div className="flex flex-col items-center gap-12">
      <div className="flex flex-row items-center gap-2 text-2xl">
        <p>To-do List</p>
        <Badge
          count={count}
          color={
            count >= 10
              ? "red"
              : count >= 5
              ? "orange"
              : count > 0
              ? "yellow"
              : null
          }
        />
      </div>
      <TodoGroup />
      <TodoGenerator />
    </div>
  );
};

export default TodoList;
