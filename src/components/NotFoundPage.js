import { Result } from "antd";

const NotFoundPage = () => {
  return (
    <div className="grow flex items-center justify-center">
      <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
      />
    </div>
  );
};

export default NotFoundPage;
