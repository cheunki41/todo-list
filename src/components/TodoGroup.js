import { useSelector } from "react-redux";
import TodoItem from "./TodoItem";
import { Empty } from "antd";

const TodoGroup = () => {
  const { todoItems } = useSelector((state) => state.todo);

  return (
    <div className="flex flex-col gap-5 items-center max-h-[40rem] overflow-y-auto">
      {todoItems.map((item) => (
        <TodoItem key={item.id} todoItem={item} />
      ))}
      {todoItems.length == 0 && (
        <Empty
          image={Empty.PRESENTED_IMAGE_SIMPLE}
          description="Create your first to-do item!"
        />
      )}
    </div>
  );
};

export default TodoGroup;
